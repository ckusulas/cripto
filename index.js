const fetchJson = require('node-fetch');

/*fetchJson("https://api.binance.com/api/v1/ticker/price?symbol=ETHUSDT")
    .then(res => res.json())
    .then(json => console.log(json));



fetchJson("https://api.binance.com/api/v1/ticker/price?symbol=BTCUSDT")
    .then(res => res.json())
    .then(json => console.log(json));*/
    



//all Cripto - all information
/*fetchJson("https://api.binance.com/api/v1/ticker/24hr")
    .then(res => res.json())
    .then(json => console.log(json));*/



//Precio de todas las CriptoMonedas al momento
/*fetchJson("https://api.binance.com/api/v1/ticker/allPrices")
    .then(res => res.json())
    .then(json => console.log(json));*/



//test4
 //Precio historico 5min Ethereum  = klines
 /*

m -> minutes; h -> hours; d -> days; w -> weeks; M -> months

    1m
    3m
    5m
    15m
    30m
    1h
    2h
    4h
    6h
    8h
    12h
    1d
    3d
    1w
    1M

Explain:
  [
    1609927500000,      // Open time
    '1105.40000000',    // Open
    '1106.87000000',    // High
    '1103.16000000',    // Low
    '1106.57000000',    // Close
    '5251.23544000',    // Volume
    1609927799999,      // Close time
    '5804662.17857410', // Quote asset volume
    3112,               // Number of trades
    '2744.82054000',    // Taker buy base asset volume
    '3033911.22756130', // Taker buy quote asset volume
    '0'                 // Ignore
  ],

 */

/*
Api usado: Binance
referencia: https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md
keypublic:
keyprivate: 
(registrarse, es gratuito)


*/




 //Precio historico 5min Ethereum   
 fetchJson("https://api.binance.com/api/v1/klines?symbol=ETHUSDT&interval=5m")
    .then(res => res.json())
    .then(json => console.log(json));